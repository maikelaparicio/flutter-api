import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_api/componentes/message_box.dart';
import 'package:flutter_api/componentes/snack_message.dart';
import 'package:flutter_api/data/api/post_api_service.dart';
import 'package:flutter_api/data/api/user_api_service.dart';
import 'package:flutter_api/data/api/user_change_notifier.dart';
import 'package:flutter_api/data/data/user_built.dart';
import 'package:provider/provider.dart';

import 'package:built_collection/built_collection.dart';

class ListUserPage extends StatefulWidget {
  @override
  _ListUserPageState createState() => _ListUserPageState();
}

class _ListUserPageState extends State<ListUserPage> {
  final _formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final lastNameController = TextEditingController();

  final _userService = UserApiService1.create();

  @override
  Widget build(BuildContext context) {
    Provider.of<UserChangeNotifier>(context, listen: false).getUsers();

    return Consumer<UserChangeNotifier>(
      builder: (context, notifier, __) {
        if (notifier.state == NotifierState.disconnected) {
          return Center(
            child: Icon(Icons.signal_wifi_off, size: 82.0,),
          );
        } else if (notifier.state == NotifierState.initial) {
          return Center(
            child: Text('Esperando para cargar datos'),
          );
        } else if (notifier.state == NotifierState.loading) {
          return Center(child: CircularProgressIndicator());
        } else {
          return notifier.users.fold(
            (failure) => Center(
              child: Text(failure.message),
            ),
            (users) => ListView(
              children: _listaMapUsers(context, users.body),
            ),
          );
        }
      },
    );
  }

  List<Widget> _listaMapUsers(
      BuildContext context, BuiltList<UserBuilt> users) {
//    UserApiService.userApiService.getUserById(1).then((UserModel value) {
//      var p = value.toJson();
//      p['name'] = 'Dart Flutter';
//      p['username'] = 'dflutter';
//      UserApiService.userApiService.updateUser(UserModel.fromJson(p));
//    });
//    UserApiService.userApiService
//        .createUser(UserModel(name: 'Dart Flutter', username: 'dflutter'));

//    UserApiService.userApiService.getUserById(2).then((UserModel value) {
//      UserApiService.userApiService.deleteUser(value);
//    });

//    UserApiService.userApiService.createUser(UserModel(name: 'Dart Flutter', username: 'dflutter'));
//    UserApiService.userApiService.updateUser(UserModel(id: 1,name: 'Dart Flutter 2do', username: 'dflutter'));

    //List p = users.map((e) => UserModel.fromJson(e)).toList();

    return users.map((_user) {
      return Dismissible(
        key: UniqueKey(),
        background: Container(
          color: Colors.redAccent,
          child: Row(
            children: [
              Icon(
                Icons.delete_forever,
                color: Colors.white,
              ),
              SizedBox(
                width: 10.0,
              ),
              Text(
                'ELIMINAR',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
        secondaryBackground: Container(
          color: Colors.greenAccent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                'ACTUALIZAR',
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(
                width: 10.0,
              ),
              Icon(
                Icons.edit,
                color: Colors.white,
              ),
            ],
          ),
        ),
        onDismissed: (direction) {
          if (direction == DismissDirection.startToEnd) {
            _delete(_user);
          } else if (direction == DismissDirection.endToStart) {
            _update(_user);
          }
        },
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(
                Icons.person_outline,
                color: Colors.black26,
              ),
              title: Text('${_user.name} (${_user.username.toLowerCase()})'),
              subtitle: Text(
                  'ID: ${_user.id} ${" " * 10}Company: ${_user.company.name}'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () {
                Navigator.pushNamed(context, 'user_details', arguments: 'user');
              },
            ),
            Divider(
              thickness: 2.0,
              color: Colors.lightBlue,
              indent: 70.0,
              endIndent: 20.0,
            )
          ],
        ),
      );
    }).toList();
  }

  void _delete(UserBuilt user) {
    messageBox(
        context: context,
        icon: Icons.delete_forever,
        title: '¿Eliminara el usuario?',
        content: Text('${user.name.toUpperCase()}'),
        onPressOkBtn: () {
          setState(() {
            // _userService.deleteUser(user.id);
            _userService.search(username: 'Bret');

            //DBProvider.db.deleteUserById(user.id);
            //database.userDao.deleteUser(user);
            //FirestoreProvider.firestoreProvider.deleteUser(docId);
          });
          Navigator.of(context).pop();
        },
        onPressCancelBtn: () {
          setState(() {});
          Navigator.of(context).pop();
        });
  }

  void _update(UserBuilt user) {
    usernameController.text = user.username;
    lastNameController.text = user.name;
    Widget form = Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            controller: usernameController,
            validator: (value) {
              return value.isEmpty
                  ? 'El campo username no puede estar vacio'
                  : null;
            },
            decoration: InputDecoration(
              hintText: 'Insert username',
              icon: Icon(Icons.person),
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          TextFormField(
            controller: lastNameController,
            validator: (value) {
              return value.isEmpty ? 'El lastName no puede estar vacio' : null;
            },
            decoration: InputDecoration(
              hintText: 'Insert lastname',
              icon: Icon(Icons.info_outline),
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          RaisedButton(
            child: Text('Actualizar'),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _userService.updateUser(user.id, UserBuilt((u) {
                  u.replace(user);
                  u
                    ..name = lastNameController.text
                    ..username = usernameController.text;
                }));
//                FirestoreProvider.firestoreProvider.updateUser(
//                    docId,
//                    User(
//                        active: user.active,
//                        name: usernameController.text,
//                        lastName: lastNameController.text));
                //DBProvider.db.updateUser(user);
//                database.userDao.updateUser(User(
//                    id: user.id,
//                    active: user.active,
//                    name: usernameController.text,
//                    lastName: lastNameController.text,
//                    created: user.created));
                setState(() {});
                Scaffold.of(context).showSnackBar(snackMessage(
                    'El usuario ${usernameController.text} ha sido actualizado'));
                _formKey.currentState?.reset();
                Navigator.of(context).pop();
              }
            },
          ),
        ],
      ),
    );
    messageBox(
      context: context,
      icon: Icons.edit,
      title: 'Actualizar el usuario',
      content: form,
      onPressCancelBtn: () {
        setState(() {});
        Navigator.of(context).pop();
      },
    );
  }
}
