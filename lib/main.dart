import 'package:flutter/material.dart';
import 'package:flutter_api/data/api/user_api_service.dart';
import 'package:flutter_api/data/api/user_change_notifier.dart';
import 'package:flutter_api/pages/home_page.dart';
import 'package:provider/provider.dart';
import 'dart:developer' as developer;

import 'package:logging/logging.dart';

void main() {
  _setupLogging();
  runApp(MyApp());
}

void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print('${rec.level.name}: ${rec.time}: ${rec.message}');
    //developer.log('${rec.level.name}: ${rec.time}: ${rec.message}', name: 'user.com.getUsers');
  });
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => UserChangeNotifier(),
        child: MaterialApp(
          title: 'Chopper Api',
          theme: ThemeData(primarySwatch: Colors.blue),
          //home: HomePage(),
          home: ChangeNotifierProvider(
            create: (_) => UserChangeNotifier(),
            child: HomePage(),
          ),
        ));
//    return Provider(
//      create: (_) => UserApiService1.create(),
//      dispose: (context, UserApiService1 service) => service.client.dispose(),
//      child: MaterialApp(
//        title: 'Chopper Api',
//        theme: ThemeData(primarySwatch: Colors.blue),
//        home: HomePage(),
//      ),
//    );

//    return MaterialApp(
//      title: 'Flutter Demo',
//      theme: ThemeData(
//        primarySwatch: Colors.blue,
//      ),
//      home: HomePage(),
//    );
  }
}
