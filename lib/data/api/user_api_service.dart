import 'package:built_collection/built_collection.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter_api/config/api_conf.dart';
import 'package:flutter_api/data/data/user_built.dart';

import '../built_value_converter.dart';

part 'user_api_service.chopper.dart';

@ChopperApi(baseUrl: 'users/')
abstract class UserApiService1 extends ChopperService {


  static UserApiService1 create() {
    final client = ChopperClient(
      baseUrl: CONFIG['API_URL'],
      services: [
        _$UserApiService1(),
      ],
      converter: BuiltValueConverter(),
      interceptors: [
        HttpLoggingInterceptor(),

            (Request request) async {
            chopperLogger.info('Method: ${request.method}');
          return request;
        },
            (Response response) async {
              chopperLogger.info('statusCode: ${response.statusCode}');
          return response;
        },

      ],
    );
    return _$UserApiService1(client);
  }

  @Get()
  Future<Response<BuiltList<UserBuilt>>> getUsers();

  @Get(path: '{id}/{username}/{idP}')
  Future<Response<UserBuilt>> getUserById(@Path('id') int id);

  @Post(headers: {"Content-Type": "application/json"})
  Future<Response<UserBuilt>> createUser(@Body() UserBuilt user);

  @Put(path: '{id}', headers: {"Content-Type": "application/json"})
  Future<Response<UserBuilt>> updateUser(@Path('id') id,
      @Body() UserBuilt user);

  @Delete(path: '{id}', headers: {"Content-Type": "application/json"})
  Future<Response> deleteUser(@Path('id') int id);

  @Get()
  Future<Response<BuiltList<UserBuilt>>> search({
    @Query('username') String username,
    @Query('id') int id,
  });
}

//class UserApiService {
//  static final UserApiService userApiService = UserApiService._();
//
//  UserApiService._();
//
//  static final String _endpoint = '${CONFIG['API_URL']}users/';
//
//  Future<List<UserModel>> getUsers() async {
//    final response = await http.get(_endpoint);
//
//    if (response.statusCode == 200) {
//      //developer.log(response.body, name: 'user.com.getUsers');
//      return json
//          .decode(response.body)
//          .map<UserModel>((json) => UserModel.fromJson(json))
//          .toList();
//    } else {
//      throw Exception('Error al cargar los users');
//    }
//  }
//
//  Future<int> getLengthUsers() async {
//    final response = await http.get(_endpoint);
//
//    if (response.statusCode == 200) {
//      int t = json
//          .decode(response.body)
//          .map<UserModel>((json) => UserModel.fromJson(json))
//          .toList()
//          .length;
//      developer.log(t.toString(), name: 'user.com.getLengthUsers');
//      return t;
//    } else {
//      throw Exception('Error al cargar los posts');
//    }
//  }
//
//  Future<UserModel> getUserById(int id) async {
//    final response = await http.get(_endpoint + id.toString());
//
//    if (response.statusCode == 200) {
//      developer.log(response.body, name: 'user.com.getUserById');
//      return UserModel.fromJson(json.decode(response.body));
//    } else {
//      throw Exception('Failed to load post');
//    }
//  }
//
//  Future createUser(UserModel user) async {
//    final response = await http.post(_endpoint,
//        headers: {"Content-Type": "application/json"}, body: json.encode(user));
//    developer.log(response.body, name: 'user.com.createUser');
//    return response.body;
//  }
//
//  Future updateUser(UserModel user) async {
//    final response = await http.put(_endpoint + user.id.toString(),
//        headers: {"Content-Type": "application/json"}, body: json.encode(user));
//    developer.log(response.body, name: 'user.com.updateUser');
//    return response.body;
//  }
//
//  Future deleteUser(UserModel user) async {
//    final response = await http.delete(_endpoint + user.id.toString(),
//        headers: {"Content-Type": "application/json"});
//    developer.log(response.statusCode.toString(), name: 'user.com.deleteUser');
//    return response.body;
//  }
//}
