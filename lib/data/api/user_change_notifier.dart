import 'package:chopper/chopper.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:built_collection/built_collection.dart';

import 'package:flutter_api/data/api/user_api_service.dart';
import 'package:flutter_api/data/data/user_built.dart';

import 'exceptions/failure.dart';
import 'exceptions/extensions.dart';

enum NotifierState { initial, loading, loaded, disconnected, connected }

class UserChangeNotifier extends ChangeNotifier {
  final _userService = UserApiService1.create();
  //final Connectivity _connectivity = Connectivity();

  NotifierState _state = NotifierState.initial;

  NotifierState get state => _state;

  void _setState(NotifierState state) {
    _state = state;
    notifyListeners();
  }

  Either<Failure, Response<UserBuilt>> _user;
  Either<Failure, Response<BuiltList<UserBuilt>>> _users;

  Either<Failure, Response<UserBuilt>> get user => _user;

  Either<Failure, Response<BuiltList<UserBuilt>>> get users => _users;

  void _setUser(Either<Failure, Response<UserBuilt>> user) {
    _user = user;
    notifyListeners();
  }

  void _setUsers(Either<Failure, Response<BuiltList<UserBuilt>>> users) {
    _users = users;
    notifyListeners();
  }

  void _checkConnection() async {
    //_setState(NotifierState.disconnected);
    _setState(NotifierState.connected);
  }

  void getUsers() async {
    await _checkConnection();
    if (state != NotifierState.disconnected) {
      _setState(NotifierState.loading);
      await Task(() => _userService.getUsers())
          .attempt()
          .mapLeftToFailure()
          .run()
          .then((value) => _setUsers(value));
      _setState(NotifierState.loaded);
    }
  }
}
