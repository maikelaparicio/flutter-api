import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_built.g.dart';

abstract class UserBuilt implements Built<UserBuilt, UserBuiltBuilder> {
  @nullable
  int get id;

  String get name;
  String get username;
  String get email;
  AddressBuilt get address;
  String get phone;
  String get website;
  CompanyBuilt get company;


  UserBuilt._();

  factory UserBuilt([updates(UserBuiltBuilder b)]) = _$UserBuilt;

  static Serializer<UserBuilt> get serializer => _$userBuiltSerializer;
}

abstract class CompanyBuilt
    implements Built<CompanyBuilt, CompanyBuiltBuilder> {
  String get name;
  String get catchPhrase;
  String get bs;

  CompanyBuilt._();

  factory CompanyBuilt([updates(CompanyBuiltBuilder b)]) = _$CompanyBuilt;

  static Serializer<CompanyBuilt> get serializer => _$companyBuiltSerializer;
}

abstract class AddressBuilt
    implements Built<AddressBuilt, AddressBuiltBuilder> {
  String get street;
  String get suite;
  String get city;
  String get zipcode;
  GeoBuilt get geo;

  AddressBuilt._();

  factory AddressBuilt([updates(AddressBuiltBuilder b)]) = _$AddressBuilt;

  static Serializer<AddressBuilt> get serializer => _$addressBuiltSerializer;
}

abstract class GeoBuilt implements Built<GeoBuilt, GeoBuiltBuilder> {
  String get lat;
  String get lng;

  GeoBuilt._();

  factory GeoBuilt([updates(GeoBuiltBuilder b)]) = _$GeoBuilt;

  static Serializer<GeoBuilt> get serializer => _$geoBuiltSerializer;
}
