import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import '../user_built.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  UserBuilt,
  CompanyBuilt,
  AddressBuilt,
  GeoBuilt,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
